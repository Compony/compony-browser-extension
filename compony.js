document.body.style.border = "0px solid green";

/*
Just draw a border round the document.body.
*/
var forEach = function (array, callback, scope) {
  for (var i = 0; i < array.length; i++) {
    callback.call(scope, i, array[i]);
  }
};

var moduleName = document.querySelector('#page-subtitle').textContent;
var urlParts = window.location.href.split('/');
var moduleMachineName = urlParts[4];

var $modules = document.querySelectorAll('.node-project-module');
forEach($modules, function (index, $module) {
  // $module.append('<div class="compony-components"><h3>Compony components</h3></div>');

  // Create a div to start working in
  var $container = document.createElement("div");
  $container.classList.add('compony-components');

  var $containerTitle = document.createElement("h3");
  $containerTitle.append('Compony components');
  $container.appendChild($containerTitle);

  // We need to fetch the api here and count how many Compony components we have
  // In case we have components for this module
  // @Todo
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == XMLHttpRequest.DONE) {
     if (xmlhttp.status == 200) {
       let response = JSON.parse(xmlhttp.responseText);
       if(response.valid) {
        if(response.amount > 0) {
          forEach(response.components, function (index, $component) {
            var $rendered_component = addComponent($component);

            // Assemble the container
            $container.appendChild($rendered_component);
          });
        } else {
          var $rendered_component = addNoResultsComponent();
          $container.appendChild($rendered_component);
        }
       } else {
         console.log(xmlhttp);
       }
     }
     else if (xmlhttp.status == 400) {
       console.log(xmlhttp);
     }
     else {
       console.log(xmlhttp);
     }
    }
  };

  xmlhttp.open("GET", 'https://www.compony.io/compony_api/getComponentsFromModuleName/' + moduleMachineName, true);
  xmlhttp.send();

  // Assemble the module
  $module.appendChild($container);
});

function createComponent() {
  var $component = document.createElement("article");
  $component.classList.add('compony-component');
  $component.style.padding = '0.2em';
  $component.style.backgroundImage = 'linear-gradient(140deg, #fa9b3f 0%, #f04d3a 100%)';
  $component.style.marginBottom = '1em';

  return $component;
}

function createComponentWithLink($url) {
  var $component = document.createElement("a");
  $component.setAttribute('href', $url);
  $component.classList.add('compony-component');
  $component.style.display = 'block';
  $component.style.padding = '0.2em 4rem 0.2rem 0.2rem';
  $component.style.backgroundImage = 'linear-gradient(140deg, #fa9b3f 0%, #f04d3a 100%)';
  $component.style.position = 'relative';
  $component.style.textDecoration = 'none';
  $component.style.color = '#000';
  $component.style.marginBottom = '1em';

  var $viewSVG = createViewSVG();
  $component.append($viewSVG);

  return $component;
}

function createComponentInner() {
  $componentInner = document.createElement("div");
  $componentInner.classList.add('compony-component__inner');
  $componentInner.style.padding = '0.8em';
  $componentInner.style.background = 'rgba(255, 255, 255, 1)';
  $componentInner.style.transition = 'all 0.2s ease';

  var css = '.compony-component:hover .compony-component__inner { background-color: rgba(255, 255, 255, 0.9) !important; }';
  var style = document.createElement('style');

  if (style.styleSheet) {
      style.styleSheet.cssText = css;
  } else {
      style.appendChild(document.createTextNode(css));
  }
  document.getElementsByTagName('head')[0].appendChild(style);

  return $componentInner;
}

function createComponentTitle() {
  var $componentTitle = document.createElement("h4");
  $componentTitle.classList.add('component__title');
  $componentTitle.style.marginTop = '0em';
  $componentTitle.style.marginBottom = '1em';

  return $componentTitle;
}

function createComponentDescription() {
  var $componentDescription = document.createElement("div");
  $componentDescription.classList.add('component__description');

  return $componentDescription;
}

function createComponentParagraph() {
  var $descriptionParagraph = document.createElement("p");
  $descriptionParagraph.classList.add('description__paragraph');
  $descriptionParagraph.classList.add('description__paragraph--no-components-yet');
  $descriptionParagraph.style.marginBottom = '0';

  return $descriptionParagraph;
}

function createDownloadStat($component_vars) {
  var $descriptionStat = document.createElement("div");
  $descriptionStat.classList.add('description__statistic');

  var $downloadSVG = createDownloadSVG();
  var $downloadNumber = createDownloadNumber($component_vars.downloads);

  $descriptionStat.append($downloadSVG);
  $descriptionStat.append($downloadNumber);

  return $descriptionStat;
}

function createAuthorStat($component_vars) {
  var $authorStat = document.createElement("div");
  $authorStat.classList.add('description__author');

  var $authorSVG = createAuthorSVG();
  var $authorName = createAuthorName('by ' + $component_vars.author);

  $authorStat.append($authorSVG);
  $authorStat.append($authorName);

  return $authorStat;
}

function createDownloadNumber($number) {
  var $downloadNumber = document.createElement("span");
  $downloadNumber.append($number);

  if($number == 1) {
    $downloadNumber.append(' dowload');
  } else {
    $downloadNumber.append(' dowloads');
  }

  return $downloadNumber;
}

function createAuthorName($name) {
  var $authorName = document.createElement("span");
  $authorName.append($name);

  return $authorName;
}

function createLink($url) {
  var $link = document.createElement("a");
  $link.setAttribute('href', $url);

  return $link;
}

function createShieldSVG() {
  var $svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
  $svg.setAttribute('viewBox', '0 0 512 512');
  $svg.classList.add('component__secured');
  $svg.style.width = '2em';
  $svg.style.verticalAlign = 'middle';
  $svg.style.marginLeft = '0.4em';
  $svg.style.fill = '#fa9a3f';

  var $path1 = document.createElementNS("http://www.w3.org/2000/svg", "path");
  $path1.setAttribute('d', 'M234.53 285.912l-64.77-61.653-27.588 28.983 94.603 90.052 144.148-160.495-29.769-26.738z');

  var $path2 = document.createElementNS("http://www.w3.org/2000/svg", "path");
  $path2.setAttribute('d', 'M420.499 65.192c-48.502-8.692-93.168-35.18-115.476-50.195C290.447 5.186 273.496 0 256.001 0s-34.447 5.186-49.022 14.996C184.671 30.011 140.005 56.5 91.503 65.192c-28.726 5.148-49.576 30.002-49.576 59.097v120.71c0 39.877 11.157 78.749 33.159 115.539 17.214 28.781 41.064 56.288 70.888 81.757 50.147 42.825 99.804 65.156 101.892 66.086L256 512l8.134-3.619c2.089-.929 51.745-23.261 101.892-66.086 29.823-25.469 53.675-52.976 70.888-81.757 22.004-36.789 33.159-75.662 33.159-115.539V124.29c.002-29.096-20.848-53.95-49.574-59.098zM430.061 245c0 59.45-30.033 115.375-89.267 166.224-34.432 29.558-69.39 48.824-84.791 56.643C220.601 449.879 81.941 371.328 81.941 245V124.29c0-9.695 6.99-17.985 16.621-19.711 55.718-9.985 105.843-39.616 130.761-56.388 7.947-5.35 17.172-8.178 26.678-8.178s18.732 2.828 26.678 8.177c24.919 16.773 75.043 46.402 130.761 56.387 9.63 1.726 16.621 10.016 16.621 19.711V245z');

  $svg.append($path1);
  $svg.append($path2);

  return $svg;
}

function createDownloadSVG() {
  var $svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
  $svg.setAttribute('viewBox', '0 0 41.712 41.712');
  $svg.classList.add('component__downloads');

  $svg.style.width = '0.8em';
  // $svg.style.verticalAlign = 'middle';
  $svg.style.marginRight = '0.7em';
  $svg.style.marginLeft = '0.1em';
  $svg.style.fill = '#000';

  var $path1 = document.createElementNS("http://www.w3.org/2000/svg", "path");
  $path1.setAttribute('d', 'M31.586 21.8a1.112 1.112 0 0 0 0-1.587 1.093 1.093 0 0 0-1.571 0l-8.047 8.032V1.706c0-.619-.492-1.127-1.111-1.127s-1.127.508-1.127 1.127v26.539l-8.031-8.032a1.112 1.112 0 0 0-1.587 0 1.112 1.112 0 0 0 0 1.587l9.952 9.952a1.14 1.14 0 0 0 1.587 0l9.935-9.952zm7.888 7.286c0-.619.492-1.111 1.111-1.111s1.127.492 1.127 1.111v10.92c0 .619-.508 1.127-1.127 1.127H1.111A1.118 1.118 0 0 1 0 40.006v-10.92c0-.619.492-1.111 1.111-1.111s1.127.492 1.127 1.111v9.809h37.236v-9.809z');

  $svg.append($path1);

  return $svg;
}

function createViewSVG() {
  var $svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
  $svg.setAttribute('viewBox', '0 0 59.2 59.2');
  $svg.classList.add('component__view');
  $svg.style.width = '2rem';
  $svg.style.verticalAlign = 'middle';
  $svg.style.marginRight = '1.2em';
  $svg.style.position = 'absolute';
  $svg.style.top = '50%';
  $svg.style.transform = 'translateY(-50%)';
  $svg.style.right = '0';
  $svg.style.fill = '#FFF';

  var $path1 = document.createElementNS("http://www.w3.org/2000/svg", "path");
  $path1.setAttribute('d', 'M51.062 21.561c-11.889-11.889-31.232-11.889-43.121 0L0 29.501l8.138 8.138c5.944 5.944 13.752 8.917 21.561 8.917s15.616-2.972 21.561-8.917l7.941-7.941-8.139-8.137zm-1.217 14.664c-11.109 11.108-29.184 11.108-40.293 0l-6.724-6.724 6.527-6.527c11.109-11.108 29.184-11.108 40.293 0l6.724 6.724-6.527 6.527z');

  var $path2 = document.createElementNS("http://www.w3.org/2000/svg", "path");
  $path2.setAttribute('d', 'M28.572 21.57c-3.86 0-7 3.14-7 7a1 1 0 0 0 2 0c0-2.757 2.243-5 5-5a1 1 0 0 0 0-2z');

  var $path3 = document.createElementNS("http://www.w3.org/2000/svg", "path");
  $path3.setAttribute('d', 'M29.572 16.57c-7.168 0-13 5.832-13 13s5.832 13 13 13 13-5.832 13-13-5.831-13-13-13zm0 24c-6.065 0-11-4.935-11-11s4.935-11 11-11 11 4.935 11 11-4.934 11-11 11z');

  $svg.append($path1);
  $svg.append($path2);
  $svg.append($path3);

  return $svg;
}

function createAuthorSVG() {
  var $svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
  $svg.setAttribute('viewBox', '0 0 502.664 502.664');
  $svg.classList.add('component__downloads');

  $svg.style.width = '1em';
  $svg.style.verticalAlign = 'sub';
  $svg.style.marginRight = '0.6em';
  $svg.style.fill = '#000';

  var $path1 = document.createElementNS("http://www.w3.org/2000/svg", "path");
  $path1.setAttribute('d', 'M153.821 358.226L0 274.337v-46.463l153.821-83.414v54.574L46.636 250.523l107.185 53.431v54.272zm26.273 29.358L282.103 115.08h32.227L212.084 387.584h-31.99zm168.749-29.358v-54.272l107.164-52.999-107.164-52.59v-53.927l153.821 83.522v46.183l-153.821 84.083z');

  $svg.append($path1);

  return $svg;
}

function assembleComponent($component, $componentInner, $componentTitle, $componentDescription) {
  $componentInner.append($componentTitle);
  $componentInner.append($componentDescription);
  $component.prepend($componentInner);

  return $component;
}

function addComponent($component_vars) {
  var $component = createComponentWithLink($component_vars.url);
  var $componentInner = createComponentInner();
  var $componentTitle = createComponentTitle();
  var $componentDescription = createComponentDescription();
  var $descriptionParagraph = createComponentParagraph();
  // var $componentLink = createLink($component_vars.url);

  $componentTitle.append($component_vars.title);

  // If the component is secure, add a shield to it.
  if($component_vars.secured == 'secure') {
    var $shieldSVG = createShieldSVG();
    $componentTitle.append($shieldSVG);
  }

  var $statDownloads = createDownloadStat($component_vars);
  $componentDescription.append($statDownloads);

  var $statAuthor = createAuthorStat($component_vars);
  $componentDescription.append($statAuthor);

  // Assemble the component
  $component = assembleComponent($component, $componentInner, $componentTitle, $componentDescription);

  return $component;
}

function addNoResultsComponent() {
  var $component = createComponent();
  var $componentInner = createComponentInner();
  var $componentTitle = createComponentTitle();
  var $componentDescription = createComponentDescription();
  var $descriptionParagraph = createComponentParagraph();

  // Fill in no results texts
  $componentTitle.append('No Compony components yet.');
  $descriptionParagraph.append('This module doesn\'t have any Compony components yet. Be the first one to commit a component linked to the ' + moduleName + ' module on Compony.io');

  // Assemble a custom description
  $componentDescription.append($descriptionParagraph);

  // Assemble the component
  $component = assembleComponent($component, $componentInner, $componentTitle, $componentDescription);

  return $component;
}
